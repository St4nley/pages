#set -e
echo "Checking for newer files online first"
git pull

git add --all .

echo "####################################"
echo "Rédigez le commit !"
echo "####################################"

read input

git commit -m "$input"

git push -u origin master


echo "################################################################"
echo "#################    Push avec succès     ######################"
echo "################################################################"


